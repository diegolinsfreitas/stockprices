# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_auto_20150128_2139'),
    ]

    operations = [
        migrations.RenameField(
            model_name='stockprice',
            old_name='close_price',
            new_name='PREULT',
        ),
    ]
