# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0009_auto_20150228_1657'),
    ]

    operations = [
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU10',
            new_name='PREABE',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU11',
            new_name='PREMAX',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU2',
            new_name='PREMED',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU3',
            new_name='PREMIN',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU4',
            new_name='PU1_ABE',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU5',
            new_name='PU1_MAX',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU6',
            new_name='PU1_MED',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='PU7',
            new_name='PU2_MIN',
        ),
        migrations.RemoveField(
            model_name='stockprice',
            name='PU8',
        ),
        migrations.RemoveField(
            model_name='stockprice',
            name='PU9',
        ),
    ]
