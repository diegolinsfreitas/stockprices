# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0007_auto_20150216_1809'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=255)),
                ('access_token', models.CharField(max_length=1000)),
                ('issued_at', models.IntegerField()),
                ('expires_in', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
