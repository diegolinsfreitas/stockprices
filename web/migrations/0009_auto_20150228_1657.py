# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0008_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockprice',
            name='stock',
            field=models.CharField(max_length=7),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='user',
            name='access_token',
            field=models.CharField(max_length=2000),
            preserve_default=True,
        ),
    ]
