# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_auto_20150128_2033'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockprice',
            name='PU1',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
