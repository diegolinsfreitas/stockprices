# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0006_auto_20150208_2251'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockprice',
            name='PU10',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU11',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU8',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU9',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
