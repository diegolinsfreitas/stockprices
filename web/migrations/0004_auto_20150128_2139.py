# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_stockprice_pu1'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockprice',
            name='PU2',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU3',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU4',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU5',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU6',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stockprice',
            name='PU7',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
