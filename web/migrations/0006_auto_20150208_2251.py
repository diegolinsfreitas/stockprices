# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0005_auto_20150208_1414'),
    ]

    operations = [
        migrations.RenameField(
            model_name='stockprice',
            old_name='pred_fivedays',
            new_name='N1',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='pred_oneday',
            new_name='N2',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='pred_threedays',
            new_name='N3',
        ),
        migrations.RenameField(
            model_name='stockprice',
            old_name='pred_twodays',
            new_name='N4',
        ),
    ]
