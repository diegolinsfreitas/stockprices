# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StockPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(verbose_name=b'date')),
                ('close_price', models.FloatField()),
                ('stock', models.CharField(max_length=5)),
                ('predicted', models.FloatField()),
                ('pred_oneday', models.FloatField()),
                ('pred_twodays', models.FloatField()),
                ('pred_threedays', models.FloatField()),
                ('pred_fivedays', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
