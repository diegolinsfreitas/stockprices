__author__ = 'diego'

from tastypie.resources import ModelResource, Resource, fields
from django.db.models import Count
from django.db import connection
from application.core.stockdao import StockDAO
from application.core.globals import *
from application.gauth import authorized

from tastypie.http import HttpForbidden
from tastypie.exceptions import ImmediateHttpResponse

class StockPriceResponse(object):
    date = None
    close_price = None
    predicted_price = None
    future_predictions = None

class UserResponse(object):
    message = None

class StockPricesResource(Resource):
    # Just like a Django ``Form`` or ``PredictionModel``, we're defining all the
    # fields we're going to handle with the API here.
    '''uuid = fields.CharField(attribute='uuid')
    user_uuid = fields.CharField(attribute='user_uuid')
    message = fields.CharField(attribute='message')
    created = fields.IntegerField(attribute='created')'''
    date = fields.DateTimeField(attribute='date')
    close_price = fields.FloatField(attribute='close_price')
    #predicted_price = fields.FloatField(attribute='predicted_price',null=True)
    future_predictions = fields.ListField(attribute='future_predictions',null=True)

    class Meta:
        resource_name = 'stockprices'
        object_class = StockPriceResponse

    def obj_get_list(self, bundle, **kwargs):
        response = []
        authUser = None
        if ON_OPENSHIFT:
            authUser = authorized(bundle)
        else:
            authUser = {'valid':True}
        if not authUser['valid']:
           raise ImmediateHttpResponse(
                    HttpForbidden(authUser['result']['error'])
                )

        stock_name = bundle.request.GET.get('stock_name')

        dao = StockDAO()

        predictions = dao.get_predictions(stock_name)
        last_idx = len(predictions) - 1
        for idx, stock_price in enumerate(predictions):
            spr = StockPriceResponse()
            spr.date = stock_price.date
            spr.close_price = stock_price.PREULT
            #spr.predicted_price = stock_price.predicted
            if idx == last_idx:
                target_results = {}
                spr.future_predictions = []
                for target in TARGETS:
                    spr.future_predictions.append({target: getattr(stock_price,target)})
            response.append(spr)
        return response

class UserResource(Resource):
    # Just like a Django ``Form`` or ``PredictionModel``, we're defining all the
    # fields we're going to handle with the API here.
    message = fields.CharField(attribute='message')

    class Meta:
        resource_name = 'users'
        object_class = UserResponse

    def obj_get(self, bundle, **kwargs):
        response = UserResponse()
        response.message = "It Works"
        return response

    def verify_signature(self,signed_data, signature_base64):
        key64 = b'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmtZJcn9qo8biMSTbBRMox1lQM6Qoji8Nsc1mZU9Hx3OJJ2s8mv1FTAJRM5sa6QGDFRWDjvV3d7ceuuqx8iCZaVPA7gfv67K+s0o71pILilFdjrlNbFCUleQL3a13cqu3OmdLjF2KNnvA8Ykvx8pDriHIfbupJifPKnAm3Rdz4b/tW6N05RnG7BThRCOVw0yeZo+jD7nIEieWVnjghcEM/lW68F3IrIFvUes1v53d2xuQYFJJaqYWo0Qg0x+LzEa5ruURASzwzs1clJTq3r46JhJ56uKcet8inpsIkD3kecvLErO5tI5OXZTLBRtQVEt+QOxxZYuRSui5uYr+ib0exQIDAQAB'

        keyDER = b64decode(key64)
        keyPub = RSA.importKey(keyDER)
        """Returns whether the given data was signed with the private key."""
        h = SHA.new(signed_data)
        verifier = PKCS1_v1_5.new(keyPub)
        return verifier.verify(h, signature_base64)
