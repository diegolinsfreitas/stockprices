from django.core.management.base import BaseCommand, CommandError
from web.models import StockPrice

class Command(BaseCommand):
    help = 'Delete all data'

    def handle(self, *args, **options):
        StockPrice.objects.all().delete()