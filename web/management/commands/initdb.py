from django.core.management.base import BaseCommand
import application.core.crawler as cw


class Command(BaseCommand):
    help = 'Loads the database with intial data of the stock prices'

    def handle(self, *args, **options):
       cw.Crawler().init_stock_db_year(args);