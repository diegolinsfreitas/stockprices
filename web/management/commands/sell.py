from django.core.management.base import BaseCommand
from datetime import datetime

from application.core.globals import *
from application.robot.portifolio import Portifolio


class Command(BaseCommand):
    help = 'Create Buy orders'

    def handle(self, *args, **options):
        current_time = datetime.now(TIMEZONE)
        time_range = (current_time.replace(hour=17, minute=55, second=0, microsecond=0), \
                      current_time.replace(hour=17, minute=59, second=59, microsecond=0))
        if time_range[0] <= current_time <= time_range[1]:
            Portifolio().sell_stocks()
        #Portifolio().sell_stocks()