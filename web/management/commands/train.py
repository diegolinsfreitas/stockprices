from django.core.management.base import BaseCommand, CommandError
from application.core.trainning import *
from optparse import make_option


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--optimize',
                    action='store_true',
                    dest='optimize',
                    default=False,
                    help='Use grid search to train models'),
    )

    def handle(self, *args, **options):
        trainner = Trainner()
        if len(args) == 1:
            trainner.train(args[0], options['optimize'])
        elif len(args) >1:
            trainner.train(None, options['optimize'], args)
        else:
            trainner.train(None, options['optimize'])