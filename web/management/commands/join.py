from django.core.management.base import BaseCommand, CommandError
from application.core.datawraling import BovespaStockFile
from optparse import make_option

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--all',
                    action='store_true',
                    dest='all',
                    default=False,
                    help='create stock history with all stocks'),
    )

    def handle(self, *args, **options):
        BovespaStockFile().save_csv_file(args,options['all'])