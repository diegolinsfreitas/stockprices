from django.core.management.base import BaseCommand, CommandError
from web.models import Sentiment
from application.core.tweeter import SentimentsLoader
import os

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    #def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        ON_OPENSHIFT = True if 'OPENSHIFT_REPO_DIR' in os.environ else False
        if ON_OPENSHIFT:
            os.chdir(os.environ['OPENSHIFT_REPO_DIR'])
        SentimentsLoader().load_sentiments_from_twitter()