from django.core.management.base import BaseCommand
import application.core.crawler as cw
import datetime

from application.core.globals import *
from application.core.trainning import *


class Command(BaseCommand):
    help = 'Loads the database with intial data of the stock prices'

    def handle(self, *args, **options):
        if len(args) != 0:
            day = args[0]
            cw.Crawler().update_stocks(day);
        else:
            current_time = datetime.datetime.now(TIMEZONE)
            time_range = (current_time.replace(hour=19, minute=0, second=0, microsecond=0), \
                          current_time.replace(hour=23, minute=59, second=59, microsecond=0))
            if time_range[0] <= current_time <= time_range[1]:
                day = current_time.strftime("%d%m%Y")
                if cw.Crawler().update_stocks(day):
                    #trainner = Trainner()
                    #trainner.train(None, True)
                    day_full = current_time.strftime("%Y-%m-%d 00:00:00")
                    Portifolio().buy_stocks(day_full)