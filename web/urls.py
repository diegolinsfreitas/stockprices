__author__ = 'diego'

from django.conf.urls import patterns, url

from web import views


from tastypie.api import Api
from resources import StockPricesResource, UserResource

api = Api(api_name='api')
api.register(StockPricesResource())
api.register(UserResource())


urlpatterns = patterns('',
    url(r'^$', views.index, name='index')
) +  api.urls
