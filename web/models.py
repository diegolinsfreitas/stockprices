from django.db import models

class StockPrice(models.Model):
    date = models.DateTimeField('date')
    CODNEG = models.CharField(max_length = 7)
    PREULT = models.FloatField()
    PREMIN = models.FloatField(null=True)
    PREMAX = models.FloatField(null=True)
    PREABE = models.FloatField(null=True)
    VOLTOT = models.FloatField(null=True)

    N1 = models.CharField(max_length = 11, null=True)
    N2 = models.CharField(max_length = 11, null=True)
    N3 = models.CharField(max_length = 11, null=True)
    N4 = models.CharField(max_length = 11, null=True)

class User(models.Model):
    email = models.CharField(max_length = 255)
    access_token = models.CharField(max_length = 2000)
    issued_at = models.IntegerField()
    expires_in = models.IntegerField()
