DADOS_SER_HIST_ = "http://www.bmfbovespa.com.br/InstDados/SerHist/"
__author__ = 'diego'

import os
from datetime import *
import urllib
import zipfile, httplib
from random import choice

from application.core.globals import *

user_agents = [
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
    'Opera/9.25 (Windows NT 5.1; U; en)',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
    'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
    'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
    'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9'
]

class MyOpener(urllib.FancyURLopener, object):
    version = choice(user_agents)

def toFloat(value):
    return float(value.lstrip()) / 100


class BovespaStockFile():
    def __init__(self):
        if not os.path.exists(COTA_HIST_PATH):
            os.makedirs(COTA_HIST_PATH)

    def download_stock_prices_for_today(self):
        now = datetime.now()
        download_date = now.strftime("%d%m20%y")
        return download_stock_prices_for_day(download_date)

    # day in format ddmmyyy
    def download_stock_prices_for_day(self, day):
        file_name = "COTAHIST_D" + day + ".ZIP"
        url = DADOS_SER_HIST_ + file_name
        return self.download_stock_prices(url, file_name)

    def download_stock_prices_for_month(self, month):
        file_name = "COTAHIST_M" + month + ".ZIP"
        url = DADOS_SER_HIST_ + file_name
        return self.download_stock_prices(url, file_name)

    def download_stock_prices_for_year(self, year):
        if year == None:
            year = datetime.now().strftime("20%y")
        file_name = "COTAHIST_A" + year + ".ZIP"
        url = DADOS_SER_HIST_ + file_name
        return self.download_stock_prices(url, file_name)

    def download_stock_prices(self, url, output_name):
        output_abs_path = COTA_HIST_PATH + output_name
        urlOpener = MyOpener()
        httplib.HTTPConnection.debuglevel = 1
        urlOpener.retrieve(url, output_abs_path)
        with zipfile.ZipFile(output_abs_path) as zf:
            zf.extractall(path=COTA_HIST_PATH)
        return COTA_HIST_PATH + output_name.replace("ZIP", "TXT")


    def join_clean_stock_history(self, file_names):
        output_raw = COTA_HIST_PATH + 'stock_history_raw.txt'
        with open(output_raw, 'w') as outfile:
            for fname in file_names:
                with open(fname) as infile:
                    for line in infile:
                        if (line.startswith('99') or line.startswith('00') or len(line) == 0):
                            continue
                        outfile.write(line)
        return output_raw

    def save_csv_file(self, file_names, all_stocks=False):
        raw_file = self.join_clean_stock_history(file_names)
        stock_history_file = COTA_HIST_PATH + "stock_history.csv"
        with open(raw_file, 'r') as cotahist:
            line = "DATA,CODNEG,PREULT,PREABE,PREMAX,PREMIN,VOLTOT\n"
            date = None
            with open(stock_history_file, 'w') as output:
                output.write(line)
                for line in cotahist:
                    new_date = line[2:10]
                    if (date != new_date):
                        date = new_date
                    stock_code = line[12:24].strip()
                    if "AMBV" in stock_code:#http://br.advfn.com/noticias/ADVNEWS/2013/artigo/59972293
                        stock_code = stock_code.replace('AMBV','ABEV')
                    if all_stocks or stock_code in STOCK_NAMES:
                        output.write("%s,%s,%f,%f,%f,%f,%f\n" % (line[2:10],
                                                                 stock_code,
                                                                 toFloat(line[108:121]),
                                                                 toFloat(line[56:69]),
                                                                 toFloat(line[69:82]),
                                                                 toFloat(line[82:95]),
                                                                 toFloat(line[170:188])
                                                                )
                        )
        return stock_history_file