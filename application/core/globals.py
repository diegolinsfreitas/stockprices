__author__ = 'diego'

import os, logging
from pytz import timezone
import numpy as np
from application.settings import ON_OPENSHIFT

TIMEZONE = timezone('Brazil/West')

COTA_HIST_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0].replace("/application","/cota_hist/")
MODELS_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0].replace("/application","/models/")

LOG_FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=LOG_FORMAT)
#LOG_EXTRA = {'clientip': '127.0.0.1', 'user': 'diego'}
lg = logging.getLogger('server')


STOCK_HISTORY_CSV = "/stock_history.csv"

# The collumns of csv file with the raw data of stock prices
BASE_HISTORY_CSV_COLUMNS = ['DATA', 'CODNEG','PREULT','PREABE','PREMAX','PREMIN','VOLTOT']

STOCKS = [
'ABCB4',
'ABEV3',
'ALPA4',
'ALSC3',
'ALUP11',
'AMAR3',
'ANIM3',
'ARTR3',
'ARZZ3',
'BBAS3',
'BBDC4',
'BBRK3',
'BBSE3',
'BEEF3',
'BEMA3',
'BPHA3',
'BRAP4',
'BRFS3',
'BRIN3',
'BRKM5',
'BRML3',
'BRPR3',
'BRSR6',
'BTOW3',
'BVMF3',
'CARD3',
'CCRO3',
'CESP6',
'CIEL3',
'CMIG4',
'CPFE3',
'CPLE6',
'CRUZ3',
'CSAN3',
'CSMG3',
'CSNA3',
'CTIP3',
'CVCB3',
'CYRE3',
'DIRR3',
'DTEX3',
'ECOR3',
'ELET3',
'ELPL4',
'EMBR3',
'ENBR3',
'EQTL3',
'ESTC3',
'EVEN3',
'EZTC3',
'FIBR3',
'FLRY3',
'GETI3',
'GETI4',
'GFSA3',
'GGBR4',
'GOAU4',
'GOLL4',
'GRND3',
'HBOR3',
'HGTX3',
'HYPE3',
'IGTA3',
'ITSA4',
'ITUB4',
'JBSS3',
'KLBN11',
'KROT3',
'LAME4',
'LEVE3',
'LIGT3',
'LINX3',
'LREN3',
'MDIA3',
'MEAL3',
'MGLU3',
'MILS3',
'MPLU3',
'MRFG3',
'MRVE3',
'MULT3',
'MYPK3',
'NATU3','ODPV3','OIBR3','OIBR4','PCAR4','PDGR3','PETR4','PNVL3','POMO4','QGEP3','QUAL3','RADL3','RAPT4',
'RENT3','RLOG3','RSID3','RUMO3','SBSP3','SEER3','SLCE3','SMLE3','SMTO3','STBP11','SULA11','SUZB5','TAEE11',
'TBLE3','TCSA3','TGMA3','TIMP3','TOTS3','TRPL4','UGPA3','USIM5','VALE3','VALE5','VIVT4','VLID3','WEGE3',]

STOCK_NAMES = STOCKS
#UPDATEMODEL
FEATURES = np.array([
                        'MKTFI_STOCH',
                        'MKTFI',
                        'WEEKDAY',
                        'ROC_PREULT',
                        'LOG_MA1',
                        'STD_STOCH_RATIO_SHORT',
                        'LOG_PREULT_ADX',
                        'MACD_SHORT',
                        'CHAIKIN_MACD',
                        'ACCDIST_LOG_PREULT',
                        'ACCDIST',
                        'CHAIKIN',
                        'RES_PREULT',
                        'SUP_PREULT',
                        'MACD',
                        'MACD_RSI',
                        'MACD_ADX',
                        'MACD_STOCHD',
                        'LOG_PREULT',
                        'LOG_VOL',
                        'LOG_PREULT_LOG_VOL',
                        'SLOPE0',
                        'MFI',
                        'MFI_SHORT',
                        'STD_SLOPE',
                        'STOCH_RATIO',
                        'RSI',
                        'KURT',
                        'COV',
                        'STOCH',
                        'STOCH_RSI',
                        'STOCH_PDI',
                        'STOCH_NDI',
                        'QUANTILE_STOCH',
                        'RSI_NDI',
                        'STD_STOCH_RATIO',
                        'ADX_QUANTILE',
                        'CV_STOCH_PDI',
                        'CV_STOCH_NDI',
                        'SKEW',
                        'MFI_PDI',
                        'MA_RATIO1_MA_RATIO',
                        'RSI_MA_RATIO',
                        'QUANTILE','ADXPDI_COR','QUANTILE1',
                        'STD','ADX_MA_RATIO1_MA_RATIO','MA_RATIO','MA_RATIO1',
                        'PDI','NDI','ADX','ADXSLOPE','ADXSLOPE_SKEW','PDISLOPE_ADX','NDISLOPE_ADX'
                       ])

TARGETS = ['N1', 'N2' ,'N3', 'N4'
]

MAX_SHIFT_DAYS = 34
