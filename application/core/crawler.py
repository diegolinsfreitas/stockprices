__author__ = 'diego'
import application.core.datawraling as dw, application.core.features as ft, application.core.stockdao as dao
import pandas
from datetime import datetime, timedelta
from application.core.globals import *

class Crawler:

    #month in format "yy"
    def init_stock_db_year(self, years, stock_names = STOCK_NAMES):

        stock_dao = dao.StockDAO()
        BDAYS = 13
        AVOID_COMPARE_MINUTES = 1

        last_date = stock_dao.get_last_update_date()
        if last_date is None:
            last_date = datetime.now(TIMEZONE)
        start =  last_date - timedelta(days=(MAX_SHIFT_DAYS + BDAYS + AVOID_COMPARE_MINUTES))

        bovespa_data = dw.BovespaStockFile();
        stock_files = []
        for year in years:
            stock_files.append(bovespa_data.download_stock_prices_for_year(year))


        stock_history_csv = bovespa_data.save_csv_file(stock_files)
        stock_csv = pandas.read_csv(stock_history_csv, parse_dates=[0], dayfirst=True)



        print start
        #stock_df = stock_df.dropna()
        stock_csv = stock_csv[stock_csv.DATA > start]# AVOID_COMPARE_MINUTES get one day more in the past to
            # use > operator insted >= because i dont want to care about minutes and seconds :)

        for stock_name in stock_names:
            sample_data = stock_csv[stock_csv.CODNEG.isin([stock_name])]
            if stock_dao.is_db_clean_for_stock(stock_name):
                stock_dao.save_stock_prices(sample_data)
            else:
                stock_dao.update_stock_prices(sample_data)

    # day in format ddmmyyyy
    def update_stocks(self, day):
        current_day = datetime.strptime(day, "%d%m%Y")
        bovespa_data = dw.BovespaStockFile()
        stock_dao = dao.StockDAO()

        is_updated = stock_dao.is_stocks_updated(current_day)
        print is_updated, current_day
        if not is_updated:
            try:
                file_name = bovespa_data.download_stock_prices_for_day(day)
                stock_history_csv = bovespa_data.save_csv_file([file_name])
                stock_csv = pandas.read_csv(stock_history_csv, parse_dates=[0], dayfirst=True)
                today_df = stock_csv[stock_csv.CODNEG.isin(STOCK_NAMES)]
                stock_dao.save_stock_prices(today_df, True)
                return True
            except IOError as e:
                print e
                lg.error('cannot download stock for day %s', current_day)
                return False
        else:
            lg.info('stocks already updated for day %s', current_day)
            return False