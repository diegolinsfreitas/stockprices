__author__ = 'diego'

import features, web.models as models, predictionmodel
from application.core.globals import *
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
from application.core.features import Feature
from pandas import DataFrame, concat


class StockDAO():
    feat = features.Feature()

    def is_stocks_updated(self, day):
        count = models.StockPrice.objects \
            .filter(date__gte=day).count()
        print count
        return count > 0

    def is_db_clean_for_stock(self,stock_name):
        count = models.StockPrice.objects \
            .filter(CODNEG__iexact=stock_name).count()
        return count == 0

    def get_last_update_date(self):
        try:
            latest = models.StockPrice.objects \
                .latest('date')
            return latest.date
        except ObjectDoesNotExist:
            pass
        return None

    def get_predictions(self, stock_name):
        current_day = datetime.now()
        result_set = models.StockPrice.objects \
                         .filter(CODNEG=stock_name, date__lte=current_day) \
                         .order_by('-date')[:10]
        predictions = result_set[::-1]
        return predictions

    def save_stock_prices(self, dataframe, predict=False):
        for index, row in dataframe.iterrows():
            parsed_date = row['DATA']
            sp = models.StockPrice(
                date=parsed_date
            )
            self.set_model_attrs(row, sp)
            if predict:
                last_stocks_df = self.get_last_stock_prices_as_df(parsed_date , sp.CODNEG)
                last_stocks_df = last_stocks_df.append([row])
                self.update_stock_price_prediction( sp, last_stocks_df)
            sp.save()


    def update_stock_prices(self, dataframe):
        for index, row in dataframe.iterrows():
            parsed_date = row['DATA']
            stock_name = row['CODNEG']
            try:
                sp = models.StockPrice.objects.get(date__gte=parsed_date, date__lte=parsed_date, CODNEG=stock_name)
                self.set_model_attrs( row, sp)
                sp.save()
            except ObjectDoesNotExist:
                print "ObjectDoesNotExist", stock_name, parsed_date

    def set_model_attrs(self, row, sp):
        for feature_name in BASE_HISTORY_CSV_COLUMNS[1:]:
            setattr(sp, feature_name, row[feature_name])

    def update_stock_price_prediction(self, stock_price, df):
        feat = Feature()
        feat.dataframe = df
        feat.generate_features()
        X = feat.dataframe[FEATURES].values[-1]
        predictions = predictionmodel.PredictionModel(stock_price.CODNEG).predict(X)
        for idx, target in enumerate(TARGETS):
            setattr(stock_price, target, predictions[idx])


    #UPDATEMODEL
    def get_last_stock_prices_as_df(self, current_day, stock_name):
        result_set = models.StockPrice.objects \
                         .filter(CODNEG=stock_name, date__lt=current_day.strftime('%Y-%m-%d %H:%M:%S')) \
                         .order_by('-date')[:MAX_SHIFT_DAYS]
        stock_df = DataFrame(columns=BASE_HISTORY_CSV_COLUMNS)
        for stock_price in result_set:
            stock_row ={
                "DATA": stock_price.date,
            }
            for field in BASE_HISTORY_CSV_COLUMNS[1:]:
                stock_row[field] = getattr(stock_price,field)

            stock_df = stock_df.append([
                    stock_row
                ])
        return stock_df.sort(['DATA'])


    def get_last_stock_price(self, current_day, stock_name):
        result_set = models.StockPrice.objects \
            .filter(CODNEG=stock_name, date__lt=current_day) \
            .order_by('-date')[0]
        return result_set
