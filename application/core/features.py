__author__ = 'diego'

from sklearn.base import BaseEstimator, TransformerMixin
from pandas.stats.moments import *
from pandas import Series
import numpy as np
from pprint import pprint

from application.research.feature_functions import *

class Feature():

    dataframe = None

    #UPDATEMODEL
    def generate_features(self):
        self.dataframe['MKTFI'] = mkfi(self.dataframe.PREMAX, self.dataframe.PREMIN, self.dataframe.VOLTOT)

        self.dataframe['WEEKDAY'] = self.dataframe.DATA.apply(get_day_week)
        self.dataframe['ACCDIST'] = acc_dist(self.dataframe.PREULT.values, self.dataframe.PREMAX.values, self.dataframe.PREMIN.values, self.dataframe.VOLTOT.values)
        self.dataframe['CHAIKIN'] =  ewma(self.dataframe['ACCDIST'], span=10) - ewma(self.dataframe['ACCDIST'], span=3)
        self.dataframe['MA3'] = ewma(self.dataframe.PREULT, span=26)
        self.dataframe['MA4'] = ewma(self.dataframe.PREULT, span=12)
        self.dataframe['MACD'] =self.dataframe['MA3'] - self.dataframe['MA4']
        self.dataframe['RES'] =rolling_apply(self.dataframe['PREMAX'].values, 5, np.amax)
        self.dataframe['SUP'] =rolling_apply(self.dataframe['PREMIN'].values, 5, np.amin)
        self.dataframe['RES_PREULT'] =ewmcorr(self.dataframe['RES'].shift(-1).values, self.dataframe['PREULT'].values, span=3)
        self.dataframe['SUP_PREULT'] =ewmcorr(self.dataframe['SUP'].shift(-1).values, self.dataframe['PREULT'].values, span=3)
        self.dataframe['ROC_PREULT'] = self.dataframe['PREULT'].pct_change(5)


        self.dataframe['LOG_VOL'] = map(math.log10, self.dataframe['VOLTOT'].values)
        self.dataframe['LOG_PREULT'] = map(math.log10, self.dataframe['PREULT'].values)
        self.dataframe['LOG_PREULT_LOG_VOL'] = ewmcorr(self.dataframe['LOG_PREULT'],self.dataframe['LOG_VOL'], span=5)
        self.dataframe['ACCDIST_LOG_PREULT'] = ewmcorr(self.dataframe['LOG_PREULT'],self.dataframe['ACCDIST'], span=5)
        self.dataframe['CHAIKIN_MACD'] = ewmcorr(self.dataframe['CHAIKIN'],self.dataframe['MACD'], span=5)
        self.dataframe['MFI'] = mfi(self.dataframe.PREULT.values, self.dataframe.PREMAX.values, self.dataframe.PREMIN.values, self.dataframe.VOLTOT.values)
        self.dataframe['MFI_SHORT'] = mfi(self.dataframe.PREULT.values, self.dataframe.PREMAX.values, self.dataframe.PREMIN.values, self.dataframe.VOLTOT.values, n=5)
        self.dataframe['STOCH'] = stochos(self.dataframe.PREULT.values,self.dataframe.PREMAX.values,self.dataframe.PREMIN.values,14)
        self.dataframe['STOCHD'] = ewma(self.dataframe['STOCH'],3)
        self.dataframe['STOCH_RATIO'] = self.dataframe['STOCH'] / self.dataframe['STOCHD']
        self.dataframe['RSI'] = rsi(self.dataframe.PREULT,8)
        self.dataframe['MA1'] = ewma(self.dataframe.PREULT, span=5)
        self.dataframe['LOG_MA1'] = map(math.log10, self.dataframe['MA1'].values)
        self.dataframe['MA2'] = ewma(self.dataframe.PREULT, span=13)
        self.dataframe['MACD_SHORT'] = self.dataframe['MA2'] - self.dataframe['MA1']
        self.dataframe['MA_RATIO'] = self.dataframe['MA1'] / self.dataframe['MA2']
        self.dataframe['RSI_MA_RATIO'] =  ewmcorr(self.dataframe['RSI'],self.dataframe['MA_RATIO'], span=8)

        self.dataframe['STOCH_RSI'] = ewmcorr(self.dataframe['STOCH'],self.dataframe['RSI'], span=8)
        self.dataframe['MACD_RSI'] = ewmcorr(self.dataframe['MACD'],self.dataframe['RSI'], span=8)
        self.dataframe['MACD_STOCHD'] = ewmcorr(self.dataframe['MACD'],self.dataframe['STOCHD'], span=8)
        self.dataframe['MKTFI_STOCH'] = ewmcorr(self.dataframe['MKTFI'],self.dataframe['STOCH'], span=5)

        self.dataframe['MA_RATIO1'] = self.dataframe['MA3'] / self.dataframe['MA4']
        self.dataframe['MA_RATIO1_MA_RATIO'] = ewmcorr(self.dataframe['MA_RATIO1'],self.dataframe['MA_RATIO'], span=3)
        self.dataframe['SLOPE0'] =rolling_apply(self.dataframe['MA1'].values, 3, slope)
        self.dataframe['SLOPE1'] =rolling_apply(self.dataframe['MA2'].values, 3, slope)
        self.dataframe['QUANTILE'] = rolling_quantile(self.dataframe.PREULT, 13, 1)
        self.dataframe['QUANTILE1'] = rolling_quantile(self.dataframe.PREULT, 5, 1, center=True)
        self.dataframe['SKEW'] = rolling_skew(self.dataframe.PREULT, 8)
        self.dataframe['KURT'] = rolling_kurt(self.dataframe.PREULT, 8)
        self.dataframe['COV'] = ewmcov(self.dataframe.PREULT, span=8)
        self.dataframe['STD'] = ewmstd(self.dataframe.PREULT, span=7)
        self.dataframe['STD_STOCH_RATIO'] =  ewmcorr(self.dataframe['STD'],self.dataframe['STOCH_RATIO'], span=10)
        self.dataframe['STD_STOCH_RATIO_SHORT'] =  ewmcorr(self.dataframe['STD'],self.dataframe['STOCH_RATIO'], span=5)
        self.dataframe['VAR'] = ewmvar(self.dataframe.PREULT, span=10)
        self.dataframe['QUANTILE_STOCH'] = ewmcorr(self.dataframe['STOCH'],self.dataframe['QUANTILE'], span=13)
        self.dataframe['ADX'],self.dataframe['PDI'], self.dataframe['NDI'] = adx(self.dataframe.DATA.values,
                                                     self.dataframe.PREULT.values,
                                                     self.dataframe.PREMAX.values,
                                                     self.dataframe.PREMIN.values,
                                                     self.dataframe.PREABE.values,8)
        self.dataframe['MACD_ADX'] = ewmcorr(self.dataframe['MACD'],self.dataframe['ADX'], span=8)
        self.dataframe['LOG_PREULT_ADX'] = ewmcorr(self.dataframe['LOG_PREULT'],self.dataframe['ADX'], span=8)
        self.dataframe['CV_STOCH_PDI'] = ewmcov(self.dataframe['STOCH'],self.dataframe['PDI'], span=13)
        self.dataframe['CV_STOCH_NDI'] = ewmcov(self.dataframe['STOCH'],self.dataframe['NDI'], span=13)
        self.dataframe['MFI_PDI'] = ewmcorr(self.dataframe['MFI'],self.dataframe['PDI'], span=3)
        self.dataframe['STOCH_PDI'] = ewmcorr(self.dataframe['STOCH'],self.dataframe['PDI'], span=13)
        self.dataframe['STOCH_NDI'] = ewmcorr(self.dataframe['STOCH'],self.dataframe['NDI'], span=13)
        self.dataframe['ADX_MA_RATIO1_MA_RATIO'] = ewmcorr(self.dataframe['ADX'],self.dataframe['MA_RATIO1_MA_RATIO'], span=5)
        self.dataframe['RSI_NDI'] = ewmcorr(self.dataframe['RSI'],self.dataframe['NDI'], span=5)
        self.dataframe['ADX_QUANTILE'] = ewmcorr(self.dataframe['ADX'],self.dataframe['QUANTILE'], span=8)
        self.dataframe['ADXSLOPE'] =rolling_apply(self.dataframe['ADX'].values, 8, slope)
        self.dataframe['PDISLOPE'] =rolling_apply(self.dataframe['PDI'].values, 8, slope)
        self.dataframe['NDISLOPE'] =rolling_apply(self.dataframe['NDI'].values, 8, slope)
        self.dataframe['ADXPDI_COR'] =  ewmcorr(self.dataframe['ADX'],self.dataframe['PDI'], span=8)
        self.dataframe['ADXSLOPE_SKEW'] = ewmcorr(self.dataframe['ADXSLOPE'].values, self.dataframe['SKEW'].values, span=5)
        self.dataframe['PDISLOPE_ADX'] = ewmcorr(self.dataframe['PDISLOPE'].values, self.dataframe['PDI'].values, span=5)
        self.dataframe['NDISLOPE_ADX'] = ewmcorr(self.dataframe['NDISLOPE'].values, self.dataframe['NDI'].values, span=5)
        self.dataframe['STD_SLOPE'] = ewmcorr(self.dataframe['SLOPE0'],self.dataframe['STD'], span=5)

        #fill missing data
        self.dataframe = self.dataframe.replace([np.inf, -np.inf], np.nan)
        self.dataframe = self.dataframe.interpolate(method='barycentric')
        self.dataframe = self.dataframe.dropna().sort(['DATA'])

    def classify_return(self,target_name,span, reference):
        NEXT_RETURN = 100.0 * ( self.dataframe.PREULT.shift(-span).values - self.dataframe.PREULT.values ) / self.dataframe.PREULT.values

        if reference == 0:
            self.dataframe.loc[NEXT_RETURN > reference, target_name] = 1
            self.dataframe.loc[NEXT_RETURN <= reference, target_name] = 0
        elif reference > 0:
            self.dataframe.loc[NEXT_RETURN >= reference, target_name] = 1
            self.dataframe.loc[NEXT_RETURN < reference, target_name] = 0


    def generate_targets(self):
        self.classify_return('N1', 5, 0)
        self.classify_return('N2', 20, 0)
        self.classify_return('N3', 5, 3)
        self.classify_return('N4', 13, 5)
        self.dataframe = self.dataframe.dropna()