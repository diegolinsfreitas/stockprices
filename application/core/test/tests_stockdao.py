from django.test import TestCase
import os, unittest, pandas, numpy, application.core.stockdao as cl, datetime
from application import settings
from web.models import StockPrice
# setup_environ(settings)
# Create your tests here.

class TestStockDAO(TestCase):
    def test_save_features(self):
        stock_csv = pandas.read_csv(os.path.abspath(os.path.dirname(__file__)) + "/stock_history.csv")
        #os.environ.setdefault("DJANGO_SETTINGS_MODULE", "application.settings")
        collector_job = cl.StockDAO()
        collector_job.save_features(stock_csv)
        self.assertEqual(13, len(StockPrice.objects.filter(stock="PETR4")))
        self.assertEqual(13, len(StockPrice.objects.filter(stock="ABEV3")))
        start_date = datetime.date(2014, 12, 29)
        end_date = datetime.date(2014, 12, 30)
        stockPrice = StockPrice.objects.filter(stock="ABEV3", date__range=(start_date, end_date))
        entry = stockPrice.values()[0]
        entry2 = stockPrice.values()[1]
        self.assertEqual(16.35, entry2['PREULT'], "But was: " + str(entry2['PREULT']))
        self.assertEqual(16.37, entry2['PU1'], "But  Was: " + str(entry2['PU1']))

    def test_get_last_stock_prices(self):
        stock_csv = pandas.read_csv(os.path.abspath(os.path.dirname(__file__)) + "/stock_history_get_last_stocks.csv")
        dao = cl.StockDAO()
        dao.save_features(stock_csv)
        current_day = datetime.date(2014, 12, 30)
        stock_name = 'PETR4'
        stock_df = dao.get_last_stock_prices_as_df(current_day, stock_name)
        print stock_df.head(7)
        self.assertListEqual( [9.66, 9.46, 9.83, 10.32, 10.97, 10.3, 10.28], stock_df['PREULT'].values.tolist())





if __name__ == '__main__':
    unittest.main()