from django.test import TestCase
import application.core.datawraling, os, unittest, pandas, application.core.features as ft, numpy
# Create your tests here.

class TestFeatures(unittest.TestCase):

    def test_generate_features(self):
        stock_csv = pandas.read_csv(os.path.abspath(os.path.dirname(__file__))+"/stock_history.csv")
        stock_csv = stock_csv[stock_csv.CODNEG.isin(['PETR4'])]
        stock_features = ft.Feature()
        stock_features.dataframe = stock_csv
        stock_features.generate_features();
        #first_row = stock_features.dataframe.head(1).iloc[0]
        #print first_row
        #self.assertEquals(first_row['PU1'],nan)
        last_row = stock_features.dataframe.tail(1).iloc[0]
        #print last_row
        self.assertEquals(last_row['PU1'], 10.279999999999999)


if __name__ == '__main__':
    unittest.main()