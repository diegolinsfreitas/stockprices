from django.test import TestCase
import application.core.datawraling as dw, os, unittest, csv

# Create your tests here.

class TestBovespaStockFile(TestCase):

    def test_download_data(self):
        bovespa_data = dw.BovespaStockFile();
        file_name = bovespa_data.download_stock_prices_for_month("122014")
        self.assertTrue(os.path.exists(file_name), file_name)

    def test_clean_data(self):
        bovespa_data = dw.BovespaStockFile();
        file_name = bovespa_data.download_stock_prices_for_month("122014")
        stock_history_csv = bovespa_data.save_csv_file([file_name])
        self.assertTrue(os.path.exists(stock_history_csv), stock_history_csv)
        with open(stock_history_csv,'r') as csvfile:
            next(csv.reader(csvfile))
            first_row = next(csv.reader(csvfile))
            self.assertEqual(first_row, ["20141201", "ABEV3", "16.010000"])


if __name__ == '__main__':
    unittest.main()