__author__ = 'diego'

from application.core.globals import *
from sklearn.externals import joblib


class PredictionModel:
    def __init__(self, stock_name):
        self.stock_name = stock_name
        self.clfs = {}
        for target in TARGETS:
            try:
                self.clfs[target] = joblib.load("%s%s/%s_%s.pkl" % (MODELS_PATH, stock_name, stock_name, target))
            except:
                self.clfs[target] = None
                pass
    def predict(self, features):
        result = []
        for target in TARGETS:
            current_clf = self.clfs[target]
            if current_clf is None:
                result.append("NA")
            else:
                pred = current_clf.predict_proba(features)
                result.append("%.2f %.2f" % (pred[0,0], pred[0,1]))
        return result