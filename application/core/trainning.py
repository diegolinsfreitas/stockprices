import shutil
from operator import *

from sklearn.externals import joblib
import pandas
from sklearn import preprocessing
from sklearn.metrics import make_scorer
from sklearn.metrics.classification import *
from sklearn.pipeline import Pipeline
from sklearn import svm
from sklearn.feature_selection import SelectFdr, SelectFpr
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV

import application.core.features as feat
from application.core.datawraling import *
from application.research.feature_functions import *

def custom_score(y_true, y_pred):
    #print classification_report(y_true, y_true)
    #return matthews_corrcoef(y_true, y_pred)
    return accuracy_score(y_true, y_pred)


class Trainner:

    parameters = [{
        #'select__k': [3, 5, 8, 13, 21],
        'clf__kernel': ['rbf','sigmoid'],
        'clf__C': [ 8, 30, 50, 80, 100]
    },{
        #'select__k': [3, 5, 8, 13, 21],
        'clf__kernel': ['poly'],
        'clf__degree': [3, 4],
        'clf__C': [ 8, 30, 50, 80, 100]
    }]

    def train_it(self,X,Y):
        scaler = preprocessing.StandardScaler()
        anova_filter = SelectFpr()
        poly = preprocessing.PolynomialFeatures(include_bias=False)
        pipeline = Pipeline([
            ('scaler', scaler),
            ('poly', poly),
            ('select', anova_filter),
            ('clf', svm.SVC(probability=True, cache_size=400) ),
        ])

        scorer = make_scorer(custom_score, greater_is_better=True)
        clf = GridSearchCV(pipeline, param_grid=self.parameters, cv=StratifiedKFold(Y, n_folds=3, shuffle=True), n_jobs=3, verbose=1, scoring=scorer, loss_func=brier_score_loss)
        clf.fit(X, Y)
        self.report(clf.grid_scores_)
        return clf

    def report(self,grid_scores, n_top=1):
        top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
        for i, score in enumerate(top_scores):
            #print("Model with rank: {0}".format(i + 1))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  score.mean_validation_score,
                  np.std(score.cv_validation_scores)))
            print("Parameters: {0}".format(score.parameters))
            print("")

    def train(self, stock_history_csv, optimize_model=False, train_stocks = STOCK_NAMES):
        start_time = datetime.datetime.now()
        if stock_history_csv is None:
            bovespa_data = BovespaStockFile()
            file_name2015 = bovespa_data.download_stock_prices_for_year("2015")
            file_name2014 = bovespa_data.download_stock_prices_for_year("2014")
            file_name2013 = bovespa_data.download_stock_prices_for_year("2013")
            #file_name2012 = bovespa_data.download_stock_prices_for_year("2012")
            stock_history_csv = bovespa_data.save_csv_file([ file_name2013, file_name2014, file_name2015])

        feature_handler = feat.Feature()
        source_df = pandas.read_csv(stock_history_csv, parse_dates=[0], dayfirst=True)



        for stock_name in train_stocks:
            if os.path.exists(MODELS_PATH+stock_name):
                shutil.rmtree(MODELS_PATH+stock_name)
            feature_handler.dataframe = source_df[source_df.CODNEG == stock_name]
            feature_handler.generate_features()
            feature_handler.generate_targets()
            X = feature_handler.dataframe[FEATURES].values


            for target in TARGETS:
                try:
                    print stock_name+ ", target classification: " +target
                    fitted_model = self.train_it(X, feature_handler.dataframe[target].values.flatten())

                    if not os.path.exists(MODELS_PATH + stock_name):
                        os.makedirs(MODELS_PATH + stock_name)

                    joblib.dump(fitted_model, "%s%s/%s_%s.pkl" % (MODELS_PATH, stock_name, stock_name, target))
                except:
                    print "fail:",stock_name, target
                    pass
        end_time = datetime.datetime.now()
        diff = end_time - start_time
        print "Total trainning time > %f min" % diff.seconds/60
