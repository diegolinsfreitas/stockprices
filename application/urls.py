from django.conf.urls import patterns, include, url, static
import web.views as views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'application.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^app/', include('web.urls')),
    url(r'^$', views.ok, name='ok')
)
