__author__ = 'diego.freitas'

from application.core.globals import STOCK_NAMES
import urllib
import HTMLParser
import feedparser
import lxml
from lxml.html import fromstring

import logging


'''stocks_names = [
    ('PETR4', 'http://www.infomoney.com.br/petrobras/rss'),
    ('VALE5', 'http://www.infomoney.com.br/vale/rss'),
    ('ABEV3', 'http://www.infomoney.com.br/ambevsa/rss'),
    ('ITUB4','http://www.infomoney.com.br/itauunibanco/rss'),
    ('BVMF3','http://www.infomoney.com.br/bmfbovespa/rss'),
    ('ITSA4','http://www.infomoney.com.br/itausa/rss'),
    ('BBDC4','http://www.infomoney.com.br/bradesco/rss'),
    ('GGBR4','http://www.infomoney.com.br/gerdau/rss'),
    ('PDGR3','http://www.infomoney.com.br/pdgrealty/rss'),
    ('USIM5','http://www.infomoney.com.br/usiminas/rss'),
    ('BBSE3','http://www.infomoney.com.br/bbseguridade/rss'),
    ('OIBR4','http://www.infomoney.com.br/oi/rss'),
    ('CCRO3','http://www.infomoney.com.br/ccr/rss'),
    ('MRVE3','http://www.infomoney.com.br/mrvengenharia/rss'),
    ('HYPE3','http://www.infomoney.com.br/hypermarcas/rss'),
    ('TIMP3','http://www.infomoney.com.br/timparticipacoes/rss')]'''


'''hit_list = [ "http://...", "...", "..." ] # list of feeds to pull down

future_calls = [Future(feedparser.parse,rss_url) for rss_url in hit_list]

feeds = [future_obj() for future_obj in future_calls]

entries = []
for feed in feeds:
   entries.extend(feed[ "items" ])

sorted_entries = sorted(entries, key=lambda entry: entry["date_parsed"])
sorted_entries.reverse() '''

h = HTMLParser.HTMLParser()

# print feed["bozo"], feed["url"], feed["version"], feed["channel"]["title"], feed["channel"]["description"], \
# feed["channel"]["link"]
with open('news_yahoo_us.csv', 'wb') as news_train_data:
    news_train_data.write('stock|date|title|summary|content\n')

for stock_name in ['VALE5']:
    rss_url = "http://feeds.finance.yahoo.com/rss/2.0/headline?s=%s.SA&region=US" % stock_name
    logging.info("Reading Rss from %s",rss_url)
    feed = feedparser.parse(rss_url)
    with open('news_yahoo_us.csv', 'a+b') as news_train_data:
        for item in feed["items"]:
            content = urllib.urlopen(item["link"]).read()
            logging.info("Reading news %s",item["title"][:15])
            doc = fromstring(content)
            doc.make_links_absolute(item["link"])
            news_content = ""
            if "uk.finance.yahoo.com" in item["link"]:
                news_content = doc.get_element_by_id("mediaarticlebody")[0].text_content()
                print news_content
            else:
                continue

            summary = item["summary"]
            if not summary:
                summary = ""
            else:
                summary = lxml.html.document_fromstring(summary).text_content().encode('utf-8')
            news_train_data.write(
                "%s|%s|%s|%s|%s\n" %
                (
                    stock_name,
                    item["published"].encode('utf-8'),
                    item["title"].encode('utf-8'),
                    summary,
                    h.unescape(news_content.replace('\n', '').replace('\r', '').encode('utf-8'))
                )
            )