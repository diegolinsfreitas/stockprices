__author__ = 'diego'

import json
import httplib2
from web.models import User

# put this in for example gauth.py
def validate_access(access_token, user):
    access_status = {}
    CLIENT_ID = "480244832353-98ov61ltgmp4o3roiq3p2jkfu7459f25.apps.googleusercontent.com"
    if access_token is not None:
        # Check that the Access Token is valid.
        url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=%s'
               % access_token)
        h = httplib2.Http()
        result = json.loads(h.request(url, 'GET')[1])
        access_status['result'] = result
        if result.get('error') is not None:
            # This is not a valid token.
            access_status['valid'] = False
            access_status['gplus_id'] = None
            access_status['message'] = 'Invalid Access Token.'
        elif result['issued_to'] != CLIENT_ID:
            # This is not meant for this app. It is VERY important to check
            # the client ID in order to prevent man-in-the-middle attacks.
            access_status['valid'] = False
            access_status['gplus_id'] = None
            access_status['message'] = 'Access Token not meant for this app.'
        else:
            access_status['valid'] = True
            access_status['email'] = result['email']
            access_status['expires_in'] = result['expires_in']
            access_status['issued_at'] = result['issued_at']
            access_status['gplus_id'] = result['user_id']
            access_status['message'] = 'Access Token is valid.'

    if access_status['valid'] and user is None:
        user = User()
        user.email = access_status['email']
        user.access_token = access_token
        user.issued_at = access_status['issued_at']
        user.expires_in = access_status['expires_in']
        user.save()
    elif access_status['valid']:
        user.access_token = access_token
        user.issued_at = access_status['issued_at']
        user.expires_in = access_status['expires_in']
        user.save()

    return access_status


def validate_token(access_token, account_email):
    user = None
    try:
        user = User.objects.get(email=account_email)
        if user.access_token == access_token:
            return {'valid':True}
        else:
            return validate_access(access_token, user)
    except User.DoesNotExist:
        return validate_access(access_token, user)

def authorized(bundle):
    if bundle.request.META.get('HTTP_AUTHORIZATION') is None:
        return {'valid':False, 'result':{'error':'No Authorization provided'}}
    return validate_token(bundle.request.META.get('HTTP_AUTHORIZATION'), bundle.request.META.get('HTTP_EMAIL'))
