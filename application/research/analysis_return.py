__author__ = 'diego'

import pandas
from application.core.globals import *
import application.core.trainning as tr
import application.core.features as feat
from sklearn import preprocessing

from feature_functions import *
from sklearn.ensemble import ExtraTreesClassifier

import numpy as np
import matplotlib.pyplot as plt

from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.cross_validation import cross_val_score, KFold, train_test_split
from sklearn.metrics import classification_report, r2_score

from estimators import build_estimator
from stocknn import RNN
from pybrain.tools.xml import NetworkReader

#feature_names = np.array(FEATURES)

target_names = np.array(['NEXT_RETURN'])

train_stocks = ['PETR4']

train = tr.Trainner()

stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/cota_hist/stock_history.csv')
feature_names = np.array(['PREULT', 'PREMED', 'PREMIN', 'PREMAX', 'PREABE', 'GAP', 'RETURN'
                          ,'PU1_MED', 'PU1_ABE', 'PU1',  'PU1_MAX', 'PU1_MIN', 'GAP1', 'RETURN1'
                          ,'PU2_MED', 'PU2_MAX', 'PU2_ABE', 'PU2', 'PU2_MIN', 'GAP2', 'RETURN2'
                          ,'MA','rsi', 'adx'
                          ,'MA_RETURN'
                           ])
feature_names = np.array([
                          'RETURN4', 'RETURN3','RETURN2','RETURN1', 'RETURN'
                           ])

num_features = len(feature_names)
def generate_features(dataframe):
    '''prices = []
    for index, row in dataframe.iterrows():
        prices.append(Price(row[["PREABE", "PREMAX", "PREMIN", "PREULT", "VOLTOT"]] ) )



    dataframe['mfi'] = mfi(prices)'''
    '''dataframe['rsi'] = rsi(dataframe.PREULT.values)
    dataframe['adx'] = adx(dataframe.DATA.values, dataframe.PREULT.values, dataframe.PREMIN.values, dataframe.PREMAX.values, dataframe.PREABE.values)
    dataframe['MA'] = exp_ma(dataframe.PREULT, window=2)

    dataframe['GAP'] = dataframe.PREULT.shift(1) - dataframe.PREABE
    dataframe['MA_RETURN'] = exp_ma(dataframe.RETURN, window=2)'''

    dataframe['RETURN'] = dataframe.PREULT - dataframe.PREABE


    '''dataframe['PU1_MED'] = dataframe.PREMED.shift(1)
    dataframe['PU1_MIN'] = dataframe.PREMIN.shift(1)
    dataframe['PU1_MAX'] = dataframe.PREMAX.shift(1)
    dataframe['PU1_ABE'] = dataframe.PREABE.shift(1)
    dataframe['PU1'] = dataframe.PREULT.shift(1)
    dataframe['GAP1'] = dataframe.GAP.shift(1)'''
    dataframe['RETURN1'] = dataframe.RETURN.shift(1)

    '''dataframe['PU2_MED'] = dataframe.PREMED.shift(2)
    dataframe['PU2_MIN'] = dataframe.PREMIN.shift(2)
    dataframe['PU2_MAX'] = dataframe.PREMAX.shift(2)
    dataframe['PU2_ABE'] = dataframe.PREABE.shift(2)
    dataframe['PU2'] = dataframe.PREULT.shift(2)
    dataframe['GAP2'] = dataframe.GAP.shift(2)'''
    dataframe['RETURN2'] = dataframe.RETURN.shift(2)
    dataframe['RETURN3'] = dataframe.RETURN.shift(3)
    dataframe['RETURN4'] = dataframe.RETURN.shift(4)

    return dataframe


def generate_targets(dataframe):
    '''dataframe['OSC'] = percent_diff(dataframe.PREABE, dataframe.PREULT)
    dataframe.loc[dataframe['OSC'] > 2.0, 'OSC_CLASS'] = 5 #more than 2%
    dataframe.loc[(dataframe['OSC'] >1.0) & (dataframe['OSC'] <= 2.0), 'OSC_CLASS'] = 4 #up to 2%
    dataframe.loc[(dataframe['OSC'] > 0.0) & (dataframe['OSC'] <= 1.0), 'OSC_CLASS'] = 3 #up to 1%
    dataframe.loc[dataframe['OSC'] == 0.0, 'OSC'] = 2 # no oscilation

    dataframe.loc[(dataframe['OSC'] <0.0) & (dataframe['OSC'] >= -1.0), 'OSC_CLASS'] = 1 #up to -1%
    dataframe.loc[dataframe['OSC'] < -1.0, 'OSC_CLASS'] = 0 #more than 1%'''
    dataframe['NEXT_RETURN'] = dataframe['RETURN'].shift(-1)
    return dataframe

def select_features(x, y):
    forest = ExtraTreesClassifier(n_estimators=300,
                                 random_state=0)
    forest.fit(x, y)
    importances = forest.feature_importances_
    std = np.std([tree.feature_importances_ for tree in forest.estimators_],
                 axis=0)
    x_indices = np.argsort(importances)[::-1]
    # Print the feature ranking
    print("Feature ranking:")
    for f in range(len(feature_names)):
        print("%d. feature %s (%f)" % (f + 1, feature_names[x_indices[f]], importances[x_indices[f]]))
    plt.figure()
    plt.title("Feature importances")
    plt.bar(range(num_features), importances[x_indices],
            color="r", yerr=std[x_indices], align="center")
    plt.xticks(range(num_features), feature_names[x_indices])
    plt.xlim([-1, num_features])
    plt.show()
    return x_indices

def outlier_cleaner(predictions, features, targets):
    #calculate the error,make it descend sort, and fetch 90% of the data
    errors = (targets-predictions)**2
    cleaned_data =zip(features,targets,errors)
    cleaned_data = sorted(cleaned_data,key=lambda x:x[2], reverse=True)
    limit = int(len(targets)*0.1)

    return cleaned_data[limit:]




STOCK_BOVESPA = [
    'PETR4', 'VALE5', 'ABEV3' 'ITUB4', 'BVMF3', 'ITSA4', 'BBDC4', 'BBAS3',
    'PETR3', 'GGBR4', 'PDGR3', 'USIM5', 'VALE3', 'BBSE3', 'OIBR4', 'CSNA3',
    'CCRO3', 'MRVE3', 'HYPE3', 'TIMP3']


for stock_name in train_stocks:
    print stocks_data.head()
    stock_df = stocks_data[stocks_data.CODNEG.isin(STOCK_BOVESPA)]


    print stock_df.head()
    #stock_df = stocks_data[stocks_data.CODNEG == 'JBSS3']
    generate_features(stock_df)
    generate_targets(stock_df)
    stock_df = stock_df.dropna()
    stock_df = stock_df.sort(['CODNEG', 'DATA'])


    dummy_codnegs = pandas.get_dummies(stock_df['CODNEG'], prefix='CN')
    stock_df = stock_df.join(dummy_codnegs)
    feature_names = np.concatenate([feature_names, dummy_codnegs.columns.values])


    slice_period = 20140801
    traindf = stock_df[(stock_df.DATA <= slice_period)]
        #trainingdf = trainingdf[(trainingdf.DATA > 20140701)]
    testdf = stock_df[(stock_df.DATA > slice_period)]
    print traindf.head(10)
    print testdf.head(10)




    codneg_binarizer =  preprocessing.LabelBinarizer()
    codneg_binarizer.fit(stock_df['CODNEG'].values)
    X_train = traindf[feature_names].values
    Y_train = traindf[target_names].values.flatten()
    #X_train = np.concatenate([X_train, codneg_binarizer.transform(traindf['CODNEG'].values)],axis=1)

    X_test = testdf[feature_names].values
    Y_test = testdf[target_names].values.flatten()
    #X_test = np.concatenate([X_test, codneg_binarizer.transform(testdf['CODNEG'].values)],axis=1)


    #min_max_scaler = preprocessing.MinMaxScaler()

    #X = np.concatenate([X, labels],axis=1)


    #selected_features = x_indices[:6]
    #print "selected features ", feature_names[selected_features]
    #for train, test in KFold(len(y), n_folds=2):

    estimator = RNN()
    #estimator.load()
    estimator.fit(X_train, Y_train)
    print "Score is ", estimator.score(X_test, Y_test)












