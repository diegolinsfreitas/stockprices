__author__ = 'diego'
from cvxopt import matrix, solvers
from operator import itemgetter
from pandas import DataFrame, concat
import numpy
import csv
import numpy as np
import pylab
from sklearn import preprocessing
import pytz


IBOVESPA = [ 'ABEV3','BBAS3','BBDC4','BBSE3','BRAP4','BRFS3',
                  'BRKM5','BRML3','BRPR3','BVMF3','CCRO3','CIEL3',
                  'CPFE3','CPLE6','CRUZ3','CSAN3','CSNA3','CTIP3','CYRE3','DTEX3',
                  'ECOR3','ELPL4','EMBR3','ENBR3','ESTC3','EVEN3',
                  'FIBR3','GFSA3','GGBR4','GOAU4','GOLL4','HGTX3','HYPE3','ITSA4',
                  'ITUB4','JBSS3','KROT3','LAME4','LIGT3',
                  'MRFG3','MRVE3','NATU3','OIBR4','PCAR4','PDGR3','PETR3','PETR4',
                  'QUAL3','RENT3','RSID3','SUZB5','TBLE3','TIMP3','UGPA3',
                  'USIM5','VALE3','VALE5','VIVT4']

SMALL_CAPS = ['ABCB4','ALPA4','ALSC3','AMAR3','ANIM3','ARTR3',
              'ARZZ3','BBRK3','BEEF3','BPHA3','BRAP4','BRIN3','BRPR3',
              'BRSR6','BTOW3','CSMG3','CVCB3','CYRE3','DIRR3','DTEX3',
              'ECOR3','ELPL4','ENBR3','EQTL3','ESTC3','EVEN3','EZTC3',
              'FLRY3','GETI3','GETI4','GFSA3','GOAU4','GRND3','HBOR3',
              'HGTX3','HRTP3','IGTA3','LEVE3','LIGT3','LINX3',
              'MGLU3','MILS3','MPLU3','MRFG3','MRVE3','MYPK3','ODPV3',
              'OIBR3','POMO4','QGEP3','QUAL3','RAPT4',
              'RSID3','SEER3','SLCE3','SMLE3','SMTO3','TCSA3',
              'TGMA3','TOTS3','TRPL4','VLID3','ALUP11','STBP11','SULA11']

IVBX = ['BRAP4','BRFS3','BRKM5','BRML3','BRPR3','CCRO3','CESP6',
        'CMIG4','CPFE3','CPLE6','CRUZ3','CSAN3','CSNA3','CTIP3',
        'CYRE3','DTEX3','ECOR3','ELET3','EMBR3','ENBR3','EQTL3',
        'ESTC3','FIBR3','GGBR4','GOAU4','GOLL4','HGTX3','HYPE3',
        'JBSS3','KLBN11','LAME4','LREN3','MRFG3','MRVE3','MULT3',
        'NATU3','OIBR4','PCAR4','POMO4','QUAL3','RADL3','RENT3',
        'RUMO3','SBSP3','SMLE3','SUZB5','TBLE3','TIMP3','UGPA3','USIM5']

STOCKS = ['ABCB4', 'ABEV3', 'ALPA4', 'ALSC3', 'ALUP11', 'AMAR3', 'ANIM3',
       'ARTR3', 'ARZZ3', 'BBAS3', 'BBDC4', 'BBRK3', 'BBSE3', 'BEEF3',
       'BPHA3', 'BRAP4', 'BRFS3', 'BRIN3', 'BRKM5', 'BRML3', 'BRPR3',
       'BRSR6', 'BTOW3', 'BVMF3', 'CCRO3', 'CESP6', 'CIEL3', 'CMIG4',
       'CPFE3', 'CPLE6', 'CRUZ3', 'CSAN3', 'CSMG3', 'CSNA3', 'CTIP3',
       'CVCB3', 'CYRE3', 'DIRR3', 'DTEX3', 'ECOR3', 'ELET3', 'ELPL4',
       'EMBR3', 'ENBR3', 'EQTL3', 'ESTC3', 'EVEN3', 'EZTC3', 'FIBR3',
       'FLRY3', 'GETI3', 'GETI4', 'GFSA3', 'GGBR4', 'GOAU4', 'GOLL4',
       'GRND3', 'HBOR3', 'HGTX3', 'HRTP3', 'HYPE3', 'IGTA3', 'ITSA4',
       'ITUB4', 'JBSS3', 'KLBN11', 'KROT3', 'LAME4', 'LEVE3', 'LIGT3',
       'LINX3', 'LREN3', 'MGLU3', 'MILS3', 'MPLU3', 'MRFG3', 'MRVE3',
       'MULT3', 'MYPK3', 'NATU3', 'ODPV3', 'OIBR3', 'OIBR4', 'PCAR4',
       'PDGR3', 'PETR3', 'PETR4', 'POMO4', 'QGEP3', 'QUAL3', 'RADL3',
       'RAPT4', 'RENT3', 'RSID3', 'SBSP3', 'SEER3', 'SLCE3',
       'SMLE3', 'SMTO3', 'STBP11', 'SULA11', 'SUZB5', 'TBLE3', 'TCSA3',
       'TGMA3', 'TIMP3', 'TOTS3', 'TRPL4', 'UGPA3', 'USIM5', 'VALE3',
       'VALE5', 'VIVT4', 'VLID3']

TOP_PICkS = ["HYPE3","EMBR3","ABEV3","BBSE3","BRFS3","CCRO3",
             "BTOW3","BRML3","CIEL3","CTIP3","EQTL3",
             "ESTC3","EZTC3","GRND3","IGTA3","ITSA4","JBSS3",
             "KROT3","LAME4","LINX3","LREN3","MPLU3","NATU3","ODPV3",
             "PCAR4","POMO4","QUAL3","RADL3","RENT3","SMLE3",
             "SMTO3","TIMP3","TOTS3","VLID3","WEGE3","TBLE3","ALPA4",
             "FIBR3","SUZB5","UGPA3","MULT3"]

#STOCK_BOVESPA = IBOVESPA
STOCK_BOVESPA = STOCKS
#csv_rdr = csv.reader(open('2014-12-28-fed-monthly-currency-data.csv'))
#raw_data = [row for row in csv_rdr]

# Transpose header metadata and turn it into a data frame
#head_data = [[] for _ in range(len(raw_data[0]))]
#for row in raw_data[:6]:
#    for j, val in enumerate(row):
#        head_data[j].append(val)

#metadata = DataFrame.from_records(data=head_data[1:], columns=head_data[0])

# Filter out NA currencies
#metadata = metadata[metadata['Currency:'] != 'NA']

# This will have the same indices as the rest of the metadata.
'''countries = []
for c in metadata['Series Description']:
    c = c.upper()
    c = c.split(' -- ')[0]
    c = c.split(' - ')[0]
    countries.append(c)
countries[1] = 'EURO AREA'''''

import pandas

def load_stocks_data():
    stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/cota_hist/stock_history.csv', parse_dates=[0],
                                  dayfirst=True)[['DATA','CODNEG','PREULT']]
    stocks_data = stocks_data[stocks_data.DATA>= '2015-02-01']
    #stocks_data = stocks_data[stocks_data.DATA<= '2014-09-30']
    return stocks_data


def calc_returns(prices_returns):
    prices_returns_cov = prices_returns.cov()
    exp_returns = concat({'mean': prices_returns.mean(), 'variance': prices_returns.std()}, axis=1)
    return exp_returns, prices_returns_cov


def prepare_analysis(stocks_data, stock_codes):
    data = np.unique(stocks_data.DATA)
    prices = pandas.DataFrame(columns=['DATA'] + stock_codes)
    prices['DATA'] = data
    for stock_code in stock_codes:
        print stock_code
        #print stock_code
        prices[stock_code] = pandas.Series(stocks_data[stocks_data.CODNEG.isin([stock_code])]['PREULT'].values, index=prices.index)
    prices_returns = prices[stock_codes].pct_change().dropna()
    exp_returns, prices_returns_cov = calc_returns(prices_returns)
    return prices_returns, prices_returns_cov, exp_returns



def load_data_simulation(stock_codes):
    stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/cota_hist/stock_history.csv', parse_dates=[0],
                                  dayfirst=True)[['DATA','CODNEG','PREULT']]
    encoder = preprocessing.LabelEncoder()
    sids = encoder.fit_transform(stock_codes)

    date = pandas.DatetimeIndex(np.unique(stocks_data.DATA))
    size = len(date)
    print "Size", size
    prices = pandas.DataFrame(columns=sids, index=date)
    prices.index = prices.index.tz_localize(pytz.utc)
    #prices['DATA'] = date
    scode_remove = []
    for sid in sids:
        #print stock_code
        scode = encoder.inverse_transform(sid)
        sprices = stocks_data[stocks_data.CODNEG.isin([scode])]['PREULT'].values
        if len(sprices) == size:
            prices[sid] = pandas.Series(sprices, index=prices.index)
            print scode, sid
        else:
            scode_remove.append(sid)
            print scode, len(sprices)
    prices = prices.dropna(axis=1, subset=scode_remove, how='all')

    print prices.describe()
    print prices.head()
    return prices

# Means are the expected returns for each currency.


class CurrencyPortfolio():

    def execute(self, data,stock_codes, prices_returns, prices_returns_cov, exp_returns):
        P = matrix(data['risk_aversion'] * prices_returns_cov.as_matrix())
        q = matrix(-exp_returns['mean'].as_matrix())
        G = matrix(0.0, (len(q),len(q)))
        G[::len(q)+1] = -1.0
        h = matrix(0.0, (len(q),1))
        A = matrix(1.0, (1,len(q)))
        b = matrix(1.0)

        solution = solvers.qp(P, q, G, h, A, b)
        expected_return = exp_returns['mean'].dot(solution['x'])[0]
        variance = sum(solution['x'] * prices_returns_cov.as_matrix().dot(solution['x']))[0]

        investments = {}
        #print len(solution['x'])
        for i, amount in enumerate(solution['x']):
            # Ignore values that appear to have converged to 0.
            if amount > 10e-5:
                investments[stock_codes[i]] = amount*100

        return {
            'risk_aversion': data['risk_aversion'],
            'investments': investments,
            'expected_return': expected_return,
            'variance': variance
        }

#import matplotlib.pyplot as plt
#import seaborn
#seaborn.regplot('mean', 'variance', exp_returns)




#from matplotlib import pyplot
#pyplot.plot(risk_aversion, [p['expected_return'] for p in portfolios], 'o-')
#pyplot.axis([-1, 21, 0.0, 0.3])
#pyplot.title('Efficient Frontier for Currency Portfolios')
#pyplot.text(9, -0.05, 'Risk Aversion')
#pyplot.text(-4, 0.18, '% Return', rotation='vertical')
#pyplot.show()


'''pyplot.plot(
    [p['expected_return'] for p in portfolios],
    [p['variance'] for p in portfolios],
    'o-'
)
pyplot.axis([0, 0.3, -0.5, 7.5])
pyplot.title('Efficient Frontier for Currency Portfolios')
pyplot.text(.1, -2, 'Expected Return (%)')
pyplot.text(-0.025, 5, 'Portfolio Return Variance', rotation='vertical')
pyplot.show()'''

def report(portfolios):
    from operator import itemgetter

    for i, p in enumerate(portfolios):
        print 'Portfolio %d: expected monthly risk_aversion=%.1f, return=%.4f, variance=%.4f' % (
            i, p['risk_aversion'], p['expected_return'], p['variance']
        )

        print "KEYS", p['investments'].keys()
        #plot_risk_return(p['expected_return'], p['variance'])
        for country, investment in sorted(p['investments'].items(), key=itemgetter(1), reverse=True):

            print '\t%5.02f%% %s' % (investment, country)
        print


def plot_risk_return(expected_return, variance):
    pylab.figure(1, facecolor='w')
    pylab.plot(variance, expected_return)
    pylab.xlabel('standard deviation')
    pylab.ylabel('expected return')
    pylab.axis([0, 0.0002, 0, 0.00015])
    pylab.title('Risk-return trade-off curve (fig 4.12)')
    pylab.yticks([0.00, 0.05, 0.10, 0.15])

    '''pylab.figure(2, facecolor='w')
    c1 = [ x[0] for x in xs ]
    c2 = [ x[0] + x[1] for x in xs ]
    c3 = [ x[0] + x[1] + x[2] for x in xs ]
    c4 = [ x[0] + x[1] + x[2] + x[3] for x in xs ]
    pylab.fill(risks + [.20], c1 + [0.0], facecolor = '#F0F0F0')
    pylab.fill(risks[-1::-1] + risks, c2[-1::-1] + c1,
        facecolor = '#D0D0D0')
    pylab.fill(risks[-1::-1] + risks, c3[-1::-1] + c2,
        facecolor = '#F0F0F0')
    pylab.fill(risks[-1::-1] + risks, c4[-1::-1] + c3,
        facecolor = '#D0D0D0')
    pylab.axis([0.0, 0.2, 0.0, 1.0])
    pylab.xlabel('standard deviation')
    pylab.ylabel('allocation')
    pylab.text(.15,.5,'x1')
    pylab.text(.10,.7,'x2')
    pylab.text(.05,.7,'x3')
    pylab.text(.01,.7,'x4')
    pylab.title('Optimal allocations (fig 4.12)')'''
    pylab.show()


def analyse(stock_codes, risk_aversion=None):
    print "Total Stocks:", len(stock_codes), "Risk:", risk_aversion
    stock_data = load_stocks_data()
    prices_returns, prices_returns_cov, exp_returns = prepare_analysis(stock_data, stock_codes)

    if risk_aversion is None:
        risk_aversion = [ra/2.0 for ra in range(20)]
    yh = CurrencyPortfolio()
    portfolios = []
    for ra in risk_aversion:
        portfolios.append(yh.execute({'risk_aversion': ra}, stock_codes, prices_returns, prices_returns_cov, exp_returns))
    report(portfolios)


if __name__ == "__main__":
    analyse(STOCKS, risk_aversion=[5, 10,20])