__author__ = 'diego'
import numpy as np
import math
import datetime

def slope(y):
    n = len(y)
    x = range(1,n+1)
    a = n * np.sum(x * y)
    b = np.sum(x)*np.sum(y)
    c = n * np.sum(np.power(x,2))
    d = math.pow(np.sum(x), 2)
    slope = (a - b) / (c - d)
    return slope

def get_day_week(date):
    return date.isoweekday()

def exp_ma(values, window=5):
    weights = np.exp(np.linspace(-1., 0., window))
    weights /= weights.sum()

    # Here, we will just allow the default since it is an EMA
    a =  np.convolve(values, weights)[:len(values)]
    a[:window] = a[window]
    return a #again, as a numpy array.

def ols_coef(prices, n=10):
    coef = np.zeros(len(prices))
    intercept = np.zeros(len(prices))
    for i in range(n, len(prices)):
        slice = prices[i-n:i+1]
        x = np.arange(0,len(slice))
        y=np.array(slice)
        z = np.polyfit(x,y,1)
        coef[i] = z[0]
        intercept[i] = z[1]
    return coef, intercept

def mov_slope(values, mov_wind=10):
    window_length = mov_wind + 2
    window = np.zeros(window_length)
    window[0] = 1
    window[window_length - 1] = -1

    slope = np.convolve(values, window, mode='valid') / float(len(window) - 1)
    padlength = len(window) - 1
    slope = np.hstack([np.ones(padlength), slope])
    return slope
#MFI
class Price(object) :
    def __init__(self, row) :
        open, high, low, close, volume = row
        self.open, self.high, self.low, self.close, self.volume = \
            float(open), float(high), float(low), float(close), float(volume)

def runningTotal(series) :
    result, s = [0], 0
    for ss in series :
        s += ss
        result.append(s)
    return result

def rsi(prices, n=14):
    deltas = np.diff(prices)
    seed = deltas[:n+1]
    down = -seed[seed<0].sum()/n
    up = seed[seed>=0].sum()/n
    rs = up/down
    rsi = np.zeros_like(prices)
    rsi[:n] = 100. - 100./(1.+rs)

    for i in range(n, len(prices)):
        delta = deltas[i-1]
        if delta>0:
            upval = delta
            downval =0
        else:
            upval = 0.
            downval = -delta

        up = (up*(n-1)+upval)/n
        down = (down * (n-1)+downval)/n

        rs = up/down
        rsi[i] = 100. - 100./(1.+rs)
    return rsi

def mkfi(high, low, volume):
    return (high - low )/volume


def mfi(close, high, low, volume, n=14):
    typ = (high + low + close) / 3
    returns = [ (typ[i] - typ[i-1]) / typ[i-1] for i in range(1, len(typ)) ]
    mf = [ typi * voli for typi, voli in zip(typ[1:], volume[1:]) ]
    pmf = [ (mfi if reti > 0 else 0) for mfi, reti in zip(mf, returns) ]
    result = [ (100.0 * p/a) for p, a in zip(summedSeries(pmf, n), summedSeries(mf, n)) ]
    return np.concatenate([np.ones(n) * result[0], result])

def acc_dist(close, high, low, volume):
    clv = (close - low) - (high - close) / (high - low)
    return clv * volume

def summedSeries (series, length) :
    s = runningTotal(series)
    for i in xrange(len(s) - length) :
        yield s[i + length] - s[i]

def stochos(close, high, low, n=14):
    stch_os =  np.empty((len(close),))
    stch_os[:] = np.nan
    for i in range(n, len(close)):
        start = i-n
        end = i+1
        current_close = close[i]
        lowest =  np.amin(low[start:end])
        highest = np.amax(high[start:end])
        stch_os[i] = (current_close - lowest)/(highest - lowest)* 100
    return stch_os

def smooth(x,window_len=5,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')

    return y

def percent_diff(current_value, past_value):
    return 100 * (past_value - current_value ) / current_value

#ADX
def TR(d,c,h,l,o,yc):
    x = h-l
    y = abs(h-yc)
    z = abs(l-yc)

    if y <= x >= z:
        TR = x
    elif x <= y >= z:
        TR = y
    elif x <= z >= y:
        TR = z
    return d, TR


def DM(d,o,h,l,c,yo,yh,yl,yc):
    moveUp = h-yh
    moveDown = yl-l

    if 0 < moveUp > moveDown:
        PDM = moveUp
    else:
        PDM = 0

    if 0 < moveDown > moveUp:
        NDM = moveDown
    else:
        NDM = 0

    return d,PDM,NDM


def calcDIs(date,closep,highp,lowp,openp,span=14):
    x = 0
    TRDates = []
    TrueRanges = []
    PosDMs = []
    NegDMs = []

    while x < len(date):
        TRDate, TrueRange = TR(date[x],closep[x],highp[x],lowp[x],openp[x],closep[x-1])
        TRDates.append(TRDate)
        TrueRanges.append(TrueRange)


        #DM(d,o,h,l,c,yo,yh,yl,yc)
        last_day = x - 1
        if last_day < 0:
            last_day = 0
        DMdate,PosDM,NegDM = DM(date[x],openp[x],highp[x],lowp[x],closep[x],openp[last_day],highp[last_day],lowp[last_day],closep[last_day])
        PosDMs.append(PosDM)
        NegDMs.append(NegDM)

        x+=1

    #print len(PosDMs)

    expPosDM = exp_ma(PosDMs,span)
    expNegDM = exp_ma(NegDMs,span)
    ATR = exp_ma(TrueRanges,span)

    xx = 0
    PDIs = []
    NDIs = []

    while xx < len(ATR):
        PDI = 100*(expPosDM[xx]/ATR[xx])
        PDIs.append(PDI)

        NDI = 100*(expNegDM[xx]/ATR[xx])
        NDIs.append(NDI)

        xx += 1

    return PDIs,NDIs


def adx(date,closep,highp,lowp,openp,span=14):
    PositiveDI,NegativeDI = calcDIs(date,closep,highp,lowp,openp,span)

    xxx = 0
    DXs = []
    while xxx < len(date):
        DX = 100*( (abs(PositiveDI[xxx]-NegativeDI[xxx])
                    /(PositiveDI[xxx]+NegativeDI[xxx])))

        DXs.append(DX)
        xxx += 1

    ADX = exp_ma(DXs,span)
    return ADX,PositiveDI,NegativeDI
