__author__ = 'diego'

import pandas
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, make_scorer, precision_score
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn import svm
from sklearn.decomposition import *
from sklearn.feature_selection import SelectFpr, f_classif
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
from sklearn.qda import QDA
from sklearn.calibration import CalibratedClassifierCV
from application.core.trainning import *
from application.core.features import *
from application.core.globals import  *
import matplotlib.pyplot as plt


target_names = TARGETS


stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/cota_hist/stock_history.csv', parse_dates=[0],
                              dayfirst=True)
#stocks_data = stocks_data[stocks_data.DATA<= '2015-05-22']
all_feature_names = FEATURES


STOCK_BOVESPA = [
            'HYPE3'
]

def report_features_rank(fpr):
    num_features = len(all_feature_names)
    importances = fpr.pvalues_
    std = np.std(fpr.scores_,
                 axis=0)
    x_indices = np.argsort(importances)
    # Print the feature ranking
    print("Feature ranking:")
    for f in range(len(all_feature_names)):
        print("%d. feature %s (%f)" % (f + 1, all_feature_names[x_indices[f]], importances[x_indices[f]]))


def analyse_data(df):
    feat = Feature()
    feat.dataframe = df

    feat.generate_targets()
    feat.generate_features()
    print feat.dataframe.tail(10)
    print feat.dataframe.describe()

    trainer = Trainner()


    X = feat.dataframe[all_feature_names].values
    Y = feat.dataframe['N4'].values.flatten()

    clf = trainer.train_it(X,Y)

    print clf.best_params_, clf.best_score_
    print "classes", clf.best_estimator_.steps[3][1].classes_
    print "Score is: ",clf.best_score_
    poly = clf.best_estimator_.steps[1][1]
    fpr = clf.best_estimator_.steps[2][1]
    selected_features = fpr.get_support(indices=True)
    print "input/output features %d/%d" % (poly.n_input_features_, poly.n_output_features_)
    print "selected features ", len(selected_features)

    #report_features_rank(fpr)


    #print zip(all_feature_names,fpr.scores_, fpr.pvalues_)
    '''clf.fit(xtrain, ytrain)
    selected_features = anova_filter.get_support(indices=True)
    yp_prob = clf.predict_proba(xtest)
    score = log_loss(ytest, yp_prob)
    yp = clf.predict(xtest)
    print classification_report(ytest, yp)
    print "Classes"
    print clf.classes_
    for index in range(10):
        print yp_prob[index], ytest[index]

    print "Score is: ",score, accuracy_score(ytest, yp)'''

if __name__ == "__main__":
    for code in STOCK_BOVESPA:
        print "@@@@@@ANALYZING",code
        analyse_data(stocks_data[stocks_data.CODNEG.isin([code])])












