from pybrain.datasets import SupervisedDataSet
from pybrain.structure.networks import recurrent
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml import NetworkWriter, NetworkReader
import math, os.path
import numpy as np

from pybrain.optimization import ParticleSwarmOptimizer, GA, CMAES, StochasticHillClimber
__author__ = 'diego.freitas'


from pybrain.structure import TanhLayer, SigmoidLayer, LSTMLayer,MDLSTMLayer
from pybrain.supervised.trainers import BackpropTrainer, RPropMinusTrainer
from sklearn.metrics import r2_score

class RNN:

    n = None

    def fit(self, X, y, cycles=20):
        features = X.shape[1]
        targets = features
        self.n = buildNetwork(features,features * 2 , targets, recurrent=True, hiddenclass=MDLSTMLayer, bias=True)

        dataset = SupervisedDataSet(features,targets)
        for index, x_row in enumerate(X):
            dataset.addSample(x_row,y[index])
        #TrainDS, TestDS = dataset.splitWithProportion(0.9)
        trainer = BackpropTrainer(self.n, dataset, verbose=True, learningrate=0.01)
        trainer.trainEpochs(cycles)
        #trainer.trainUntilConvergence()

        #ga = GA(dataset.evaluateModuleMSE, self.n, verbose=True)
        #result = ga.learn()
        #print result
        #self.n = result[0]

        #ga = ParticleSwarmOptimizer(dataset.evaluateModuleMSE, self.n, verbose=True, size=30)
        #result = ga.learn()
        #self.n = result[0]

        #ga = CMAES(dataset.evaluateModuleMSE, self.n, verbose=True, size=30)
        #result = ga.learn()
        #self.n = result[0]

        #ga = StochasticHillClimber(dataset.evaluateModuleMSE, self.n, verbose=True, size=30)
        #result = ga.learn()
        #self.n = result[0]

        #return trainer.testOnData(TestDS, True)

    def predict(self,features):
        return [self.n.activate(input)[0] for input in features]

    def score(self, X, y):
        return r2_score(y, self.predict(X))

    def save(self, filename='neuralnet.xml'):
       NetworkWriter.writeToFile(self.n, filename)

    def load(self, filename = 'neuralnet_result.xml'):
        self.n = NetworkReader.readFrom(filename)
