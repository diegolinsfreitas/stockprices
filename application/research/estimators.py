__author__ = 'diego'

from sklearn import svm, neighbors
import numpy as np
import math, os.path


from lasagne.layers import DenseLayer
from lasagne.layers import InputLayer
from lasagne.layers import DropoutLayer
from lasagne.nonlinearities import softmax, tanh
from lasagne.updates import nesterov_momentum
from nolearn.lasagne import NeuralNet
from sklearn.ensemble.forest import ExtraTreesRegressor

import itertools
from sklearn.metrics import r2_score

from application.core.globals import FEATURES

def build_estimator(name='nusvr'):
    return estimators[name]()


def build_nu_svr():
    return svm.NuSVR(C=50, kernel='rbf', nu=0.9)

def build_knr():
    return neighbors.KNeighborsRegressor(5, weights='distance')

def build_nusvc():
    return svm.NuSVC(nu=0.1)

def build_erf():
    return ExtraTreesRegressor(n_estimators=100)


def build_dbn():
    num_classes = 6
    num_features = 10
    layers0 = [ ('input', InputLayer),
            ('hidden1', DenseLayer),
            ('dropout1', DropoutLayer),
            ('hidden2', DenseLayer),
            ('output', DenseLayer)
    ]

    net0 = NeuralNet(layers=layers0,
                 input_shape=(None, num_features),
                 hidden1_num_units=num_features,
                 hidden2_num_units=num_features,
                 dropout_p=0.5,
                 output_num_units=6,
                 output_nonlinearity=softmax,
                 regression=False,
                 update=nesterov_momentum,
                 update_learning_rate=0.01,
                 update_momentum=0.9,
                 eval_size=0.2,
                 verbose=1,
                 max_epochs=10)
    return net0


estimators = {
    'nusvr': build_nu_svr,
    'nusvc': build_nusvc,
    'knr': build_knr,
    'dbn': build_dbn,
    'erf': build_erf
}