__author__ = 'diego'

import pandas
import math
from application.core.globals import *
import application.core.trainning as tr
import application.core.features as feat
from sklearn import preprocessing
import matplotlib.pyplot as plt

from pandas.stats.moments import *


from feature_functions import *


import numpy as np
import matplotlib.pyplot as plt

#feature_names = np.array(FEATURES)

target_names = np.array(['NEXT_OSC'])

train_stocks = ['PETR4']

train = tr.Trainner()

stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/cota_hist/stock_history.csv', parse_dates=[0],
                              dayfirst=True)

def generate_features(dataframe):
    dataframe['MA0'] = ewma(dataframe.PREULT, span=17)
    dataframe['MA1'] = ewma(dataframe.PREULT, span=34)
    dataframe['MA2'] = ewma(dataframe.PREULT, span=50)
    #dataframe['MA3'] = ewma(dataframe.PREULT, span=3)
    dataframe['PREMEDMA'] = ewma(dataframe.PREMED, span=4)
    dataframe['STD'] = ewmstd(dataframe.PREULT,span=17)
    dataframe['STOPGAIN'] = dataframe['MA0'] + dataframe['STD']
    dataframe['SLOPE'] =rolling_apply(dataframe.PREMED, 10, slope)

    return dataframe

def slope(y):
    n = len(y)
    x = range(1,n+1)
    a = n * np.sum(x * y)
    b = np.sum(x)*np.sum(y)
    c = n * np.sum(np.power(x,2))
    d = math.pow(np.sum(x), 2)
    slope = (a - b) / (c - d)
    return slope

def trend(y):
    if y[0] < y[1]:
        return 1
    else:
        return 0

def generate_targets(dataframe):
    #dataframe['OSC'] = percent_diff(dataframe.PREABE, dataframe.PREULT)
    #dataframe.loc[dataframe['TREND'] == 1, 'TREND_DIR'] = 1 #more than 2%
    #dataframe.loc[dataframe['TREND'] == 0, 'TREND_DIR'] = 0
    #dataframe.loc[(dataframe['OSC'] <0.6), 'OSC_CLASS'] = 0 #up to 2%
    dataframe['TREND'] =rolling_apply(dataframe.SLOPE, 2, trend)
    dataframe['NEXT_TREND'] =dataframe['TREND'].shift(-3)
    '''dataframe.loc[(dataframe['OSC'] > 0.0) & (dataframe['OSC'] <= 1.0), 'OSC_CLASS'] = 3 #up to 1%
    dataframe.loc[dataframe['OSC'] == 0.0, 'OSC'] = 2 # no oscilation

    dataframe.loc[(dataframe['OSC'] <0.0) & (dataframe['OSC'] >= -1.0), 'OSC_CLASS'] = 1 #up to -1%
    dataframe.loc[dataframe['OSC'] < -1.0, 'OSC_CLASS'] = 0 #more than 1%'''
    #dataframe['NEXT_OSC'] = dataframe['OSC_CLASS'].shift(-1)
    return dataframe





def load_df(stock_list):
    stock_df = stocks_data[stocks_data.CODNEG.isin(stock_list)]

    generate_features(stock_df)
    generate_targets(stock_df)
    return stock_df.dropna()


df = load_df(['GOLL4'])

print df.describe()

plt.figure()
df.plot(x='DATA', y='PREULT')
df.plot(x='DATA', y='PREMEDMA')
#df.plot(x='DATA', y='MA0')
#df.plot(x='DATA', y='MA1')
#df.plot(x='DATA', y='MA2')
plt.plot_date(x=df['DATA'], y=df['TREND'])
#plt.plot_date(x=df['DATA'], y=df['SLOPE'])
plt.show()
#selected_feat_names, features_index = analyse_data(X, Y)










