__author__ = 'diego'

from cvxopt import matrix, solvers
from operator import itemgetter
from pandas import DataFrame, concat
import numpy
import csv
import numpy as np
import pandas
from mpt import *

TOP_PICkS = ["HYPE3","EMBR3","ABEV3","ANIM3","BBSE3","BRFS3","CCRO3",
             "BTOW3","BRML3","CIEL3","CTIP3","CVCB3","EQTL3",
             "ESTC3","EZTC3","GRND3","IGTA3","ITSA4","JBSS3","KLBN11",
             "KROT3","LAME4","LINX3","LREN3","MPLU3","NATU3","ODPV3",
             "PCAR4","POMO4","QUAL3","RADL3","RENT3","SEER3","SMLE3",
             "SMTO3","TIMP3","TOTS3","VLID3","WEGE3","TBLE3","ALPA4",
             "FIBR3","SUZB5","UGPA3","MULT3"]

#Removidos "CRUZ3"
RECOMENDACAO = ["ALPA4",
"BBDC4",
"BRFS3"	,
"EMBR3"	,
"FIBR3"	,
"HYPE3"	,
"ITSA4"	,
"ITUB4"	,
"JBSS3"	,
"KLBN11",
"ODPV3"	,
"RADL3"	,
"SMTO3",
"SUZB5"	,
"UGPA3"]

MILIONARIO = ["COCE5",
"CSAN3",
"CTIP3"	,
"GRND3"	,
"ITUB4"	,
"PARC3",
"VLID3"
 ]

BARGANHAS = [ "FLRY3",
              "VLID3",
              "COCE5",
              "LEVE3",
              "ITUB4",
              "TECN3",
              "PCAR4",
              "PRBC4",
              "ABCB4"
]



RATIO = [
    "RADL3",
    "LREN3",
    "ELPL4",
    "SANB11",
    "WEGE3",
    "VLID3",
    "BEEF3",
    "EQTL3",
    "HYPE3"

]
stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/application/research/fundamentus.csv')#[['PAPEL', 'EV_EBIT', 'ROIC', 'CRESC']]
#stocks_data = stocks_data[(stocks_data.P_L >= 0)]
print stocks_data.describe()
#print stocks_data.describe()
stocks_data = stocks_data[stocks_data.PAPEL.isin(BARGANHAS)]
stocks_data['EV_EBIT_RANK'] = stocks_data['EV_EBIT'].rank(ascending=True)
stocks_data['ROIC_RANK'] = stocks_data['ROIC'].rank(ascending=False)
stocks_data['MRGLIQ_RANK'] = stocks_data['MRGLIQ'].rank(ascending=False)
stocks_data['P_VP_RANK'] = stocks_data['P_VP'].rank(ascending=True)
stocks_data['P_L_RANK'] = stocks_data['P_L'].rank(ascending=True)
#stocks_data['DY_RANK'] = stocks_data['DY'].rank(ascending=False)


stocks_data['ROE_RANK'] = stocks_data['ROE'].rank(ascending=False)
stocks_data['CRES_RANK'] = stocks_data['CRESC'].rank(ascending=False)
stocks_data['LIQCORR_RANK'] = stocks_data['LIQCORR'].rank(ascending=False)

#Config 1 ROIC EV_EBIT MRGLIQ ROE
#Config 2 ROIC EV_EBIT MRGLIQ
#CONFIG 3 ROIC EV_EBIT
#CONFIG 4 ROIC EV_EBIT RECOMENDACAO
stocks_data['MAGIC_RANK'] = stocks_data['ROIC_RANK'] + \
                            stocks_data['EV_EBIT_RANK']
                            #stocks_data['EV_EBIT_RANK']
                            #stocks_data['P_VP_RANK'] + \
                            #stocks_data['MRGLIQ_RANK']
                            #stocks_data['ROE_RANK']

                            ##stocks_data['P_VP_RANK'] + \

                            #stocks_data['LIQCORR_RANK'] + \

                            #stocks_data['CRES_RANK']

stocks_data = stocks_data.sort('MAGIC_RANK')
print stocks_data[["PAPEL","P_L","P_VP","EV_EBIT","ROE","ROIC","MRGLIQ","MAGIC_RANK"]].head(40)

#analyse(stocks_data.PAPEL.values[:10], risk_aversion=[9])
analyse(stocks_data.PAPEL.values, risk_aversion=[30])
#analyse(stocks_data.PAPEL.values[:5], risk_aversion=[9])
#analyse(stocks_data.PAPEL.values[:75], risk_aversion=[20])
