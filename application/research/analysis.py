__author__ = 'diego'
import pickle
import pandas
from application.core.globals import *
import application.core.trainning as tr
import application.core.features as feat
from sklearn import preprocessing

from feature_functions import *
from sklearn.ensemble import ExtraTreesRegressor

import numpy as np
import matplotlib.pyplot as plt

from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.cross_validation import cross_val_score, KFold

from estimators import build_estimator

#feature_names = np.array(FEATURES)

target_names = TARGETS

train_stocks = ['PETR4']
from stocknn import RNN

train = tr.Trainner()

stocks_data = pandas.read_csv('/home/diego/dev/gitrepo/stockprices/cota_hist/stock_history.csv', parse_dates=[0],
                              dayfirst=True)
MAIN_FEATURE_NAMES = np.array(['PREULT', 'PREMED', 'PREMIN', 'PREMAX', 'PREABE', 'GAP', 'RETURN'
                            ,'PU1_MED', 'PU1_ABE', 'PU1',  'PU1_MAX', 'PU1_MIN', 'GAP1', 'RETURN1'
                            ,'PU2_MED', 'PU2_MAX', 'PU2_ABE', 'PU2', 'PU2_MIN', 'GAP2', 'RETURN2'
                            ,'MA0','MA1', 'MA2', 'MA_ABE0', 'ABOVE_MA1'
                            ,'rsi', 'adx'
                           ])

num_features = len(MAIN_FEATURE_NAMES)
def generate_features(dataframe):
    dataframe['rsi'] = rsi(dataframe.PREULT.values)
    dataframe['adx'] = adx(dataframe.DATA.values, dataframe.PREULT.values, dataframe.PREMIN.values, dataframe.PREMAX.values, dataframe.PREABE.values)
    dataframe['MA0'] = exp_ma(dataframe.PREULT, window=3)
    dataframe['MA1'] = exp_ma(dataframe.PREULT, window=17)
    dataframe['MA2'] = exp_ma(dataframe.PREULT, window=34)
    dataframe['MA_ABE0'] = exp_ma(dataframe.PREABE, window=3)


    dataframe.loc[dataframe['MA1'] > dataframe['PREULT'], 'ABOVE_MA1'] = 1 #more than 2%
    dataframe.loc[dataframe['MA1'] < dataframe['PREULT'], 'ABOVE_MA1'] = 0 #more than 2%

    dataframe['RETURN'] = dataframe.PREULT - dataframe.PREABE
    dataframe['GAP'] = dataframe.PREULT.shift(1) - dataframe.PREABE

    dataframe['PU1_MED'] = dataframe.PREMED.shift(1)
    dataframe['PU1_MIN'] = dataframe.PREMIN.shift(1)
    dataframe['PU1_MAX'] = dataframe.PREMAX.shift(1)
    dataframe['PU1_ABE'] = dataframe.PREABE.shift(1)
    dataframe['PU1'] = dataframe.PREULT.shift(1)
    dataframe['GAP1'] = dataframe.GAP.shift(1)
    dataframe['RETURN1'] = dataframe.RETURN.shift(1)

    dataframe['PU2_MED'] = dataframe.PREMED.shift(2)
    dataframe['PU2_MIN'] = dataframe.PREMIN.shift(2)
    dataframe['PU2_MAX'] = dataframe.PREMAX.shift(2)
    dataframe['PU2_ABE'] = dataframe.PREABE.shift(2)
    dataframe['PU2'] = dataframe.PREULT.shift(2)
    dataframe['GAP2'] = dataframe.GAP.shift(2)
    dataframe['RETURN2'] = dataframe.RETURN.shift(2)

    return dataframe


def generate_targets(dataframe):
    featurizer = feat.Feature()
    featurizer.dataframe = dataframe
    featurizer.generate_targets()
    return dataframe

def select_features(x, y, names=MAIN_FEATURE_NAMES):
    num_features =len(names)
    forest = ExtraTreesRegressor(n_estimators=300,
                                 random_state=0)
    forest.fit(x, y)
    importances = forest.feature_importances_
    std = np.std([tree.feature_importances_ for tree in forest.estimators_],
                 axis=0)
    x_indices = np.argsort(importances)[::-1]
    # Print the feature ranking
    print("Feature ranking:")
    for f in range(len(names)):
        print("%d. feature %s (%f)" % (f + 1, names[x_indices[f]], importances[x_indices[f]]))
    plt.figure()
    plt.title("Feature importances")
    plt.bar(range(num_features), importances[x_indices],
            color="r", yerr=std[x_indices], align="center")
    plt.xticks(range(num_features), names[x_indices])
    plt.xlim([-1, num_features])
    plt.show()
    return x_indices

def outlier_cleaner(predictions, features, targets):
    #calculate the error,make it descend sort, and fetch 90% of the data
    errors = (targets-predictions)**2
    cleaned_data =zip(features,targets,errors)
    cleaned_data = sorted(cleaned_data,key=lambda x:x[2], reverse=True)
    limit = int(len(targets)*0.1)

    return cleaned_data[limit:]

def analyse_data(x, y):
    x = np.array(x)
    y = np.array(y)

    '''poly_feat = preprocessing.PolynomialFeatures(interaction_only=True)
    x = poly_feat.fit_transform(x)
    poly_names = np.array(["POLY_%d" % i for i in range(poly_feat.powers_.shape[0]-len(MAIN_FEATURE_NAMES))])
    feature_names = np.concatenate([MAIN_FEATURE_NAMES, poly_names])
    print 'Features shape',feature_names,  x.shape
    x_indices = select_features(x, y, feature_names)'''

    x_indices = select_features(x, y)
    scores = []
    scores_cleaned = []
    selected_features = x_indices[:10]
    print "selected features ", MAIN_FEATURE_NAMES[selected_features]
    for train, test in KFold(len(y), n_folds=4):
        xtrain, xtest, ytrain, ytest = x[train], x[test], y[train], y[test]
        xtrain = xtrain[:, selected_features]
        xtest = xtest[:, selected_features]

        estimator =  build_estimator('nusvr')
        estimator.fit(xtrain, ytrain)
        scores.append(estimator.score(xtest, ytest))

        '''
        cleaned_d = outlier_cleaner(estimator.predict(xtrain),xtrain,ytrain)
        x_cleaned = np.array([e[0] for e in cleaned_d])
        y_cleaned = np.array([e[1] for e in cleaned_d])

        estimator = build_estimator()
        estimator.fit(x_cleaned, y_cleaned)
        scores_cleaned.append(estimator.score(xtest, ytest))
        '''
        yp = estimator.predict(xtest)

        plt.plot(yp, ytest, 'o')
        plt.plot(ytest, ytest, 'r-')

    plt.xlabel("Predicted")
    plt.ylabel("Observed")
    print "Score is ", scores, np.mean(scores)
    #print "Score without outliers is ", scores_cleaned, np.mean(scores_cleaned)
    plt.show()



def run():
    for stock_name in train_stocks:
        print 'analyzing ', stock_name
        print stocks_data.tail()
        stock_df = stocks_data[stocks_data.CODNEG == stock_name]

        generate_features(stock_df)
        generate_targets(stock_df)
        stock_df = stock_df.dropna()
        x, n_y = train.vectorize(stock_df, MAIN_FEATURE_NAMES, target_names)
        min_max_scaler = preprocessing.MinMaxScaler()
        x = min_max_scaler.fit_transform(x)
        analyse_data(x, n_y[target_names[0]])



        '''data = stock_df[np.concatenate([feature_names, target_names])].values
        min_max_scaler = preprocessing.MinMaxScaler()
        scaled_data = min_max_scaler.fit_transform(data)
        analyse_data(scaled_data[:, 0:-4],scaled_data[:,-4:-3].flatten())'''

if __name__ == '__main__':
    run()










